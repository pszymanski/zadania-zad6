# -*- coding: utf-8 -*-

from flask import Flask, render_template
import bookdb

app = Flask(__name__)
app.config['DEBUG'] = True

db = bookdb.BookDB()


@app.route('/')
def books():
    return render_template('book_list.html', book_list=db.titles())


@app.route('/book/<book_id>/')
def book(book_id):
    return render_template('book_detail.html', book=db.title_info(book_id))

if __name__ == '__main__':
    app.run(debug=True)
