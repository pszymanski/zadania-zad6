
Laboratorium #6
===============

1. Wykorzystując framework Flask stworzyć aplikację z poprzedniej pracy domowej.
   Uzupełnij główny plik aplikacji ``book_app.py`` i szablony w podkatalogu ``templates``.
   Aplikacja powinna współdziałać z *Apachem* na serwerze laboratoryjnym.

2. Zmodyfikować aplikację tak, by korzystała z relacyjnej bazy danych *sqlite* oraz
   napisać testy jednostkowe w pliku ``book_app_test.py`` za pomocą modułu *unittest*
   sprawdzające poprawność działania aplikacji we wszystkich możliwych aspektach.